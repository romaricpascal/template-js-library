describe('cypress', () => {
  it('is setup', () => {
    // The blank page offers the library pre-loaded on the page
    // as `window.library`
    cy.visit('cypress/fixtures/blank.html');
    cy.window().then(({ library }) => {
      expect(library.version).to.eq('0.1.0');
    });
  });
});
