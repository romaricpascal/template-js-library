module.exports = {
  extends: [
    'eslint:recommended',
    'plugin:import/recommended',
    'plugin:prettier/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  rules: {
    // Because the .prettierrc file is `.cjs`
    'prettier/prettier': ['error', require('./.prettierrc.cjs')],
    // Force extensions in imports to ensure library work OK when `import`ed in browser
    'import/extensions': ['error', 'ignorePackages'],
    // Ensure imports are ordered so they're easier to manage
    'import/order': ['error'],
    // Little file housekeeping to ensure imports come first
    // Note: exporting last is not such a great idea. It prevents const related
    // to specific exports to be grouped with what they relate to (without
    // having to create a module)
    'import/first': ['error'],
    // Force named exports, to avoid issues exporting a dual CommonJS/ESM package
    // https://nodejs.org/dist/latest-v16.x/docs/api/packages.html#dual-commonjses-module-packages
    // (see Writing dual packages while avoiding or minimizing hazards)
    // but also as it enforces naming the imports consistently
    'import/no-default-export': ['error'],
    'no-shadow': ['error', { builtinGlobals: true, hoist: 'all', allow: [] }],
  },
  overrides: [
    {
      files: ['*.test.js'],
      extends: ['plugin:mocha/recommended'],
      parserOptions: {
        // Re-set ecmaVersion that's being unset somehow
        ecmaVersion: 2020,
      },
      env: {
        browser: false,
      },
    },
    {
      files: ['cypress/**/*.js', '**/*.cy.js'],
      env: {
        browser: false,
      },
      extends: ['plugin:cypress/recommended'],
    },
    // Config files usually require a default export, so we're disabling forcing
    // named exports for them
    {
      files: ['.*.{cjs,mjs,js}', '*.config.{cjs,mjs,js}'],
      rules: {
        'import/no-default-export': ['off'],
      },
    },
  ],
};
